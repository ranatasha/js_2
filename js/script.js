/* Задание на урок:

1) Автоматизировать вопросы пользователю про фильмы при помощи цикла

2) Сделать так, чтобы пользователь не мог оставить ответ в виде пустой строки,
отменить ответ или ввести название фильма длинее, чем 50 символов. Если это происходит - 
возвращаем пользователя к вопросам опять

3) При помощи условий проверить  personalMovieDB.count, и если он меньше 10 - вывести сообщение
"Просмотрено довольно мало фильмов", если от 10 до 30 - "Вы классический зритель", а если больше - 
"Вы киноман". А если не подошло ни к одному варианту - "Произошла ошибка"

4) Потренироваться и переписать цикл еще двумя способами*/

'use strict';

const numberOfFilms = +prompt("Сколько фильмов вы уже посмотрели?", "");

const personalMovieDB = {
    count: numberOfFilms,
    movies: {},
    actors: {},
    genres: [],
    privat: false
};

for (let i = 0; i < 2; i++) {
	const lastWatchedFilm = prompt("Один из последних просмотренных фильмов?", ""),
          filmRating = prompt("На сколько оцените его?", "");

    if (lastWatchedFilm != null && filmRating != null
    	&& lastWatchedFilm != "" && filmRating != "" 
    	&& lastWatchedFilm.length <= 50 && filmRating.length <=50)  {
    	personalMovieDB.movies[lastWatchedFilm] = filmRating;
    } else {
    	console.log("the user entered the data incorrectly");
    	i--;
    }
}

// 1ый доп способ
// let i = 1;
// while (i >= 0) {
// 	const lastWatchedFilm = prompt("Один из последних просмотренных фильмов?", ""),
//           filmRating = prompt("На сколько оцените его?", "");

//     if (lastWatchedFilm != null && filmRating != null
//     		&& lastWatchedFilm != "" && filmRating != "" 
//     		&& lastWatchedFilm.length <= 50 && filmRating.length <=50)  {
//     	personalMovieDB.movies[lastWatchedFilm] = filmRating;
//     } else {
//     	console.log("the user entered the data incorrectly");
//     	i++;
//     }

//     i--;
// }

// 2ой доп способ
// let i = 2
// do {
// 	const lastWatchedFilm = prompt("Один из последних просмотренных фильмов?", ""),
//            filmRating = prompt("На сколько оцените его?", "");

//      if (lastWatchedFilm != null && filmRating != null
//      		&& lastWatchedFilm != "" && filmRating != "" 
//      		&& lastWatchedFilm.length <= 50 && filmRating.length <=50)  {
//      	personalMovieDB.movies[lastWatchedFilm] = filmRating;
//      } else {
//      	console.log("the user entered the data incorrectly");
//      	i++;
//      }

//      i--;
// } while (i > 0);

if (personalMovieDB.count < 10) {
	alert("Просмотрено довольно мало фильмов");
} else if (personalMovieDB.count >= 10 && personalMovieDB.count <=30) {
	alert("Вы классический зритель");
} else if (personalMovieDB.count > 30) {
	alert("Вы киноман");
} else {
	console.log("Произошла ошибка");
}

console.log(personalMovieDB);

// Код возьмите из предыдущего домашнего задания